import type {EditorStoreSettings} from "../../storeEditor"

export const editorSettingsStorageKey = 'editor.settings'


export function mergeItem(itemA: Object|{}, ItemB: Object|{}) {
    return { ...itemA, ...ItemB }
}

function parseValue<T>(value: string | null, defaultValue: T): T {

    if (value === null) return defaultValue

    var result
    try {
        result = JSON.parse(value)
    } catch (e) {
        result = defaultValue
    }
    return result;
}

// Persist editor settings for 
export function toPersist(data: Object, storages: Storage[]) {

    storages.forEach(storage => {
        for (const [key, value] of Object.entries(data)) {
            console.log("SET ITEM ", JSON.stringify(value))
            storage.setItem(key, JSON.stringify(value))
        }
    })

}

interface PersistData {
    [editorSettingsStorageKey] : EditorStoreSettings
}

function baseData(){
    var data : PersistData = {
        "editor.settings"  : {} as EditorStoreSettings
    }
    return data 
}


export function fromPersist(storages: Storage[]) {
    var data: PersistData 
    data = baseData()
    
    // merge settings in order 
    storages.forEach(storage => {
        for (const key of Object.keys(data)){
            let settings = storage.getItem(key)
            let settings_parsed = parseValue(settings, null)
            if(settings_parsed !== null ){
                    (data as any)[key]  = mergeItem((data as any)[key] ?? {}, settings_parsed) 
            }
        }
    })

    return data;
}
