
import LZUTF8 from '../lib/lzutf8'

// import { getActiveVersion } from './store'

import { version } from "vue"
import { ReplStore2 } from 'src/storeEditor'

function getActiveVersion() {
  return version
}


interface PackagedData {
  v: string // version
  t: number // type
  d: any // data
  c: number // compress
}

const VERSION_LATEST = '' //  empty - default to latest
//@ts-ignore
const COMPRESSION_PLAIN = 0
const COMPRESSION_LZ11 = 1
const TYPE_EMBEDDED = 0

const compressedPathPrefix = '/s/'
const githubPathPrefix = '/gist/'

/**
 *
 * @param data      files data
 * @param version   vue version
 * @param type
 * @param compression
 * @returns
 */
export function packageData(
  data: object | string,
  version = VERSION_LATEST,
  type = TYPE_EMBEDDED,
  compress = COMPRESSION_LZ11
) {
  var result = {
    v: version,
    t: type,
    d: data,
    c: compress
  }
  return result
}

function isLegacy(str: string): boolean {
  return str.substr(0, 14) === 'eyJBcHAudnVlIj'
}

function compress(myObject: object): string {
  const myString = JSON.stringify(myObject)
  return LZUTF8.compress(myString, { outputEncoding: 'Base64' }) as string
}
function decompress(compressed: string): object {
  const myString = LZUTF8.decompress(compressed, {
    inputEncoding: 'Base64',
    outputEncoding: 'String'
  }) as string
  const myObject = JSON.parse(myString)
  return myObject
}

async function load(payload: string, store: ReplStore2): Promise<PackagedData> {
  if (isLegacy(payload)) {
    var result = JSON.parse(atob(payload))
    result = packageData(result, VERSION_LATEST) // set version to empty, so it will default to current commit when the url is upgraded
  } else {
    if (payload.substr(0, 3) == compressedPathPrefix) { // #/s/:compressedPayload
      payload = payload.slice(3)
      result = decompress(payload)
    } else if (payload.startsWith(githubPathPrefix)) { // #/gist/:gistId
      // load gist using plugin loader
      var gistId = payload.split(githubPathPrefix)[1]
      let githubLoader = store.getLoader('github')
      result = await githubLoader.load(gistId)
    }
  }
  return result
}

function save(payload: object): string {
  var packagedData = packageData(
    payload,
    getActiveVersion(),
    TYPE_EMBEDDED,
    COMPRESSION_LZ11
  )
  if (packagedData.v.startsWith('@')) {
    // remove current version, to save few bytes
    packagedData.v = VERSION_LATEST
  }
  return compressedPathPrefix + compress(packagedData) // /s/...
}

export default {
  load,
  save
}
