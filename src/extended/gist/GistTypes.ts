// gistSaver.ts

 export type githubGistStoredType = github_gists_localstorage

type github_gists_localstorage = {
    description: string,
    id: string,
    files: {
        [fn: string]: {
            filename: string
            type: string,
            language: string,
            raw_url: string
            size: number
        }
    },
    owner: {
        login: string,
        avatar_url: string,
        id: string
    },
    updated_at: string,
    created_at: string
}


// gistStore.ts

export type LocalGist = {
    id: string,
    url: string,
    name: string,
    created_at: string,
    owner: GistOwner
}


export type GistOwner = {
    avatar_url: string,
    id: string,
    login: string
}


export type GithubUser = {
    login: string | null,
    token: string | null,
    avatar_url: string | null,
    id?: string | null
}


export type userInfoLocalStorage = GithubUser



