import { reactive } from "vue"
import { GetAllGistsRequest } from "./GithubClient"


import type {GistOwner, GithubUser, LocalGist, userInfoLocalStorage}   from './GistTypes'

import {
    deleteGistWithConfirm,
    saveGithubTokenToLocalStorage,
    clearGithubTokenLocalStorage,
    removeGithubUserInfoFromLocalStorage,
    getGithubUserInfoFromLocalStorage,
    getGithubTokenFromLocalStorage,
    pullAllSfcGists
} from './loader/gistSaver'



interface GistStore {
    gists: LocalGist[],
    user: GithubUser,
    lastGistId: string
}

export var gistStore: GistStore = reactive({
    gists: [],
    user: { login: "", token: "", avatar_url: "", id: "" },
    lastGistId: ""
})

window.__GISTSTORE = gistStore


export function getLastGistId() {
    return gistStore.lastGistId
}


export function setLastGistId(lastGistId: string) {
    return gistStore.lastGistId = lastGistId
}

export function setAuthenticatedUser(login: string, token: string) {
    gistStore.user.login = login
    gistStore.user.token = token
}
export function setGithubAuthToken(token: string) {
    gistStore.user.token = token
    saveGithubTokenToLocalStorage(token)
}
export function getUserToken() {
    return gistStore.user.token
}




export function setGithubUserInfo(
    login: string | null = null,
    token: string | null = null,
    avatar_url: string | null = null,
    id: string | null = null

) {
    if (login !== null) gistStore.user.login = login
    if (token !== null) gistStore.user.token = token
    if (avatar_url != null) gistStore.user.avatar_url = avatar_url
    if (id !== null) gistStore.user.id = id
    saveGithubUserInfoToLocalStorage()
}
export function resetGithubUserInfo() {

    gistStore.user = {
        ...gistStore.user,
        token: "",
        login: "",
        avatar_url: "",
        id: ""
    }
}

function saveGithubUserInfoToLocalStorage() {
    window.localStorage.setItem('github-user', JSON.stringify(gistStore.user))
}

export function getGithubUser(): GithubUser{
    return gistStore.user
}

export function signoutUser() {
    resetGithubUserInfo()
    clearGithubTokenLocalStorage()
    removeGithubUserInfoFromLocalStorage()
}





export function addGistToStore(gist: LocalGist) {
    gistStore.gists.push(gist)
    saveToLocal()
}

export async function deleteGist(token: string | null, gist_id: string) {
    var result = await deleteGistWithConfirm(token, gist_id, "")

    if (result == "NOT_FOUND" || result == "DELETED") {
        console.log("delete success, removing from local")
        deleteLocalGist(gist_id)
        saveToLocal()
        return true;
    } else {
        console.warn("unexpected result calling deleteGist")
    }
    return false;

}

export function deleteLocalGist(gist_id: string) {
    gistStore.gists = gistStore.gists.filter((v) => v.id != gist_id)
    console.log(gistStore.gists)
}


export function loadGistsFromLocal(): { gists: LocalGist[] } {
    var local_gist = window.localStorage.getItem('local-gist')
    return local_gist ? JSON.parse(local_gist) : null
}

export function saveToLocal() {
    window.localStorage.setItem('local-gist', JSON.stringify({ gists: gistStore.gists }))
}



export function syncFromLocal() {
    var fromLocal = loadGistsFromLocal()
    if (fromLocal) {
        gistStore.gists = fromLocal.gists
    }

    var userFromLocal: GithubUser = getGithubUserInfoFromLocalStorage()
    if (userFromLocal !== null) {
        gistStore.user.avatar_url = userFromLocal.avatar_url
        gistStore.user.login = userFromLocal.login
        gistStore.user.token = userFromLocal.token
    }

    gistStore.user.token = getGithubTokenFromLocalStorage()

}

export function createSfcUrlFromGistId(gistId: string, owner: string) {
    return window.location.protocol + "//" + window.location.host + "/#/gist/" + gistId + "?owner=" + owner

}
/**
 * pullFromGithub
 * pull all sfc gists from the authenticated github account.
 * and transform to the format 
 * 
 */
export async function pullFromGithub() {
    var allGists = await pullAllSfcGists()

    if (!allGists) {
        return null
    }

    allGists = allGists.map((v): LocalGist => {

        let { avatar_url, id, login } = v.owner
        var owner: GistOwner = { avatar_url, id, login }

        return {
            url: createSfcUrlFromGistId(v.id, v.owner.login),
            name: v.description.replace('#sfcVue3 | ', ""),
            id: v.id,
            created_at: v.created_at,
            owner: owner
        }
    })
    console.log("Pulled gist and transformed", allGists)
    gistStore.gists = allGists
    saveToLocal()
}


// initial 
syncFromLocal()





