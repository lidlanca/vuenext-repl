import type {GistResult} from './gistLoader'

//TODO: from repl store we need  getFiles()
//import { exportFiles } from "../../store"

import { createGistRequest, deleteGistRequest, FetchGistRequest, patchGistRequest, GetAllGistsRequest } from '../GithubClient'
import { getGithubUser } from '../GistStore'
import { githubGistStoredType, GithubUser } from '../GistTypes'

type GISTFiles = { [fn: string]: { content?: string } }


// local storage crud
export function clearGithubTokenLocalStorage() {
    removeGithubUserInfoFromLocalStorage()
    return window.localStorage.removeItem('github-token')
}

export function getGithubTokenFromLocalStorage() {
    return window.localStorage.getItem('github-token')
}

export function saveGithubTokenToLocalStorage(token: string) {
    return window.localStorage.setItem('github-token', token)
}


export function removeGithubUserInfoFromLocalStorage() {
    window.localStorage.removeItem('github-user')
}

export function getGithubUserInfoFromLocalStorage(): GithubUser {
    var github_user = window.localStorage.getItem('github-user');
    return github_user ? JSON.parse(github_user) : null
}


export function getGithubAllGistsFromLocalStorage() {
    var github_user = window.localStorage.getItem('github-all-gists');
    return github_user ? JSON.parse(github_user) : null
}


type github_gists_localstorage = {
    description: string,
    id: string,
    files: {
        [fn: string]: {
            filename: string
            type: string,
            language: string,
            raw_url: string
            size: 167
        }
    },
    owner: {
        login: string,
        avatar_url: string,
        id: string
    },
    updated_at: string,
    created_at: string
}
/**
 * pull all gists 
 *  - take only gists created by sfc 
 *  - store all gists in "github-all-gists" local storage key
 *  - remove gists by same owner that do not exists on remote.
 * 
 * 
 * @returns 
 */
export async function pullAllSfcGists() {
    var githubUser = getGithubUser()

    var githubAllgists = getGithubAllGistsFromLocalStorage() ?? []

    var gists = await GetAllGistsRequest(null, 50, 0)
    if (gists === null) {
        console.warn("could not fetch gists, user may need to authorize")
        return
    }

    // filter only gists tagged with #sfcVue3
    gists = gists.filter((v: any) => v.description.startsWith('#sfcVue3'))

    console.log(gists)

    var newIds = gists.map((v: any, idx: number) => [v.id, v])
    var newIdxToIndex = Object.fromEntries(newIds)

    var oldIds: [string, githubGistStoredType][] = githubAllgists.map((v: githubGistStoredType) => [v.id, v])

    
    // remove ids that do not exists in new 
    oldIds = oldIds.filter((v) => {
        
        if(v[1].owner.login != githubUser.login){
            return true; // gist is not owned by current user. we keep it in the gist list.
        }
        var is_in_new = v[0] in newIdxToIndex
        // console.log("is in new", v[0], is_in_new, v[1].owner.login)
        return is_in_new;
    })

    var oldIdsToIndex = Object.fromEntries(oldIds)

    // merge old and new
    var merged = { ...oldIdsToIndex, ...newIdxToIndex }

    var merged_array = Object.values(merged);

    // transform/shape the result  { description, files, owner, updated_at, created_at }
    merged_array = merged_array.map((v: github_gists_localstorage) => {
        let { id, description, files, owner, updated_at, created_at } = v
        return { id, description, files, owner, updated_at, created_at }
    })

    window.localStorage.setItem('github-all-gists', JSON.stringify(merged_array))
    return merged_array

}



/**
 * transform  files into the github format 
 * 
 * @param files 
 * @returns 
 */
function toGistFiles(files: { [fn: string]: string }): GISTFiles {

    var gistFiles: GISTFiles = {}

    for (const [fn, v] of Object.entries(files)) {
        gistFiles[fn] = {
            content: v
        }
    }
    return gistFiles
}


export async function createNewGist(token: string | null, files: {}, description: string = 'vue sfc - playground') {
    return await createGistRequest(token, toGistFiles(files), description)
}


/**
 * Make a request to github to delete a gist 
 * @param token 
 * @param gist_id 
 * @param gist_name 
 * @returns 
 */
export async function deleteGistWithConfirm(token: string | null, gist_id: string, gist_name: string) {
    var confirmed = confirm("This action will delete the gist from your github account\n Gist id:" + gist_id
        + "\n" + gist_name)

    if (confirmed) {
        return await deleteGistRequest(token, gist_id)
    }
    return false;
}


/**
 * Fetch remote gist files, and compare against local active files (exportFiles).
 * set deleted files to no options ( empty {}) which indicate gitgub to deleted the file.
 * 
 * @param gist_id 
 * @param files 
 * @returns 
 */
export async function diffForPatch(
    gist_id: string,
    files: Record<string, string> | null = null,
    remoteFiles: Record<string, string> | null = null) {

    var gist: GistResult

    if (!remoteFiles) {
        gist = await FetchGistRequest(gist_id)
    }

    var localFiles = files // ?? exportFiles()

    var localFileNames = Object.keys(localFiles!)
    var remoteFileNames = Object.keys(remoteFiles ?? gist!.files)

    // find deleted files from local, that exists on remote.
    var deletedFiles = []
    for (let remoteFileName of remoteFileNames) {
        if (!localFileNames.includes(remoteFileName)) {
            deletedFiles.push(remoteFileName)
        }
    }

    var gistFiles: GISTFiles = {}
    gistFiles = toGistFiles(localFiles!)

    // set empty for files that were deleted for the patch action.
    for (let deletedFile of deletedFiles) {
        gistFiles[deletedFile] = {}
    }

    return gistFiles

}

// perform a 
export async function patchGistWithConfirm(
    token: string | null,
    gist_id: string,
    files: {},
    description: string | null = null,
    remoteFiles: {} | null = null
) {

    var confirmed = confirm(`Are you sure you want to push the current playground files to the selected gist.
this action can result in override/delete of files in the gist.`)

    if (!confirmed) {
        return
    }

    var result = patchGistRequest(token, gist_id, files, description)
    alert("patch successful")
    return result
}

