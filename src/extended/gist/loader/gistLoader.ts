
import { FetchGistRequest } from '../GithubClient'

import { packageData } from '../../../extended/state/storeLoader'


export async function loadGist(gistId:string){
   var r = await FetchGistRequest(gistId)
   return gistResultToPackageData(r)
    
}
export interface GistResult {
    files: {
     [fileName:string] : { content:string  }
    }[]
}

function gistResultToPackageData(result: GistResult) {
    var files:{ [fn:string]:{}} = {}

     for (const [fn,v] of Object.entries(result.files)){
         files[fn] = v.content 
     }
     return  packageData(files,"",1,0 )
}