

import { getUserToken } from '../gist/GistStore'
import { diffForPatch } from './loader/gistSaver'


type delete_response = "DELETED" | "NOT_FOUND" | null

export async function deleteGistRequest(token: string | null, gist_id: string): Promise<delete_response> {
    var url = `https://api.github.com/gists/${gist_id}`

    var token = token ?? getUserToken()
    if (!token) {
        console.warn("deleteGist: no token")
        return null
    }

    var headers = {
        'user-agent': 'gister',
        "Accept": "application/vnd.github.v3+json",
        "Authorization": `token ${token}`,
        'Content-Type': 'application/json',
    }

    try {

        var response: Response = await fetch(url, {
            method: "DELETE",
            headers,
        })

        if (response.status >= 400) {
            console.warn("deleteGist, error deleting gist")
            var er = await response.json()
            if (response.status == 404) {
                return 'NOT_FOUND'
            }
            throw Error(er.message)
        }
        if (response.status == 204) {
            return 'DELETED'
        } else if (response!.status == 404) {
            return 'NOT_FOUND'
        }
        else {
            console.log("deleteGist: unexpected response ")
        }

    } catch (requestError) {
        console.error(requestError)
    }

    return null

}


export async function patchGistRequest(
    token: string | null,
    gist_id: string,
    files: {},
    description: string | null = null,
    remoteFiles = null) {


    var patchedGistFiles = await diffForPatch(gist_id, files, remoteFiles)

    var url = `https://api.github.com/gists/${gist_id}`

    var token = token ?? getUserToken()

    if (!token) {
        console.warn("createGist: no token")
        return null
    }
    var headers = {
        'user-agent': 'gister',
        "Accept": "application/vnd.github.v3+json",
        "Authorization": `token ${token}`,
        'Content-Type': 'application/json',
    }
    var _body : {files:{}, description?:string  } = {files: patchedGistFiles}
    if(description !== null){
        _body.description = "#sfcVue3 | " + description
    }
    var body = JSON.stringify(_body)
    
    console.log("PATCH BODY", body)


    var response = await fetch(url, {
        method: "PATCH",
        headers,
        body: body

    })
    if (response.status >= 400) {

        var er = await response.json()
        throw Error(er.message)
    }
    var r = await response.json()

    return r

}




export async function createGistRequest(token: string | null, files: {}, description: string = 'vue sfc - playground') {

    var url = "https://api.github.com/gists"

    var token = token ?? getUserToken()

    if (!token) {
        console.warn("createGist: no token")
        return null
    }
    var headers = {
        'user-agent': 'gister',
        "Accept": "application/vnd.github.v3+json",
        "Authorization": `token ${token}`,
        'Content-Type': 'application/json',
    }

    var body = JSON.stringify({
        files: files ,
        description: "#sfcVue3 | " + description
    })

    var response = await fetch(url, {
        method: "POST",
        headers,
        body: body

    })
    if (response.status >= 400) {

        var er = await response.json()
        throw Error(er.message)
    }
    var r = await response.json()

    return r

}




type githubUserResponse = { login: string, avatar_url: string, id:string  }

export async function getAuthorizedUserRequest(token: string | null = null): Promise<githubUserResponse | null> {
    var url = "https://api.github.com/user"
    token = token ?? getUserToken()
    if (!token) {
        return null
    }

    var headers = {
        'user-agent': 'gister',
        "Accept": "application/vnd.github.v3+json",
        "Authorization": `token ${token}`,
        'Content-Type': 'application/json',
    }
    var r = await (await fetch(url, {
        method: "GET",
        headers,

    })).json()
    return r
}


export async function FetchGistRequest(gistId:string, token?:string | null ){
    var url = `https://api.github.com/gists/${gistId}`
    
    token = token ?? getUserToken()
    if (!token) {
        console.log("no github token, request may be rate limited.")
    }

    var headers : {[k:string]:string} = {
      "Accept": "application/vnd.github.v3+json",
      // "Authorization": `token ${token}`
    };
    if(token){
        headers.Authorization =  `token ${token}`
    }

    try {
        var r = await(await fetch(url, {headers}) ).json()
    } catch (e){
        console.log(e);
    }
    
    return r
}



export async function GetAllGistsRequest( token:string|null, per_page:number = 25, page:number=0 ){
    token = token ?? getUserToken()
    if (!token) {
        return null
    }
    var url = `https://api.github.com/gists?per_page=${per_page}&page=${page}`
    var headers = {
        'user-agent': 'gister',
        "Accept": "application/vnd.github.v3+json",
        "Authorization": `token ${token}`,
        'Content-Type': 'application/json',
    }
    var r = await (await fetch(url, {
        method: "GET",
        headers,

    })).json()

    if(r.length==per_page){
        var r2 = await GetAllGistsRequest(token,per_page,page+1)
        r = [...r,...r2]
    }
    return r 
}