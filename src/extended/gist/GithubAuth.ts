
/**
 * authWithGitHub initiate an oauth flow, using netlify as "proxy" provider 
 * 
 * @returns 
 */
 export async function authWithGitHub() : Promise<{token:string}>  {
    return new Promise((resolve, reject) => {

        var authenticator = new netlify.default({
            //authenticator({
            site_id: '03bcf7ef-dbf5-430e-9049-2765b28779b3',
        })

        authenticator.authenticate(
            { provider: 'github', scope: 'gist' },
            async function (err:any, data:any) {
                if (err) {
                    reject(err)
                }

                resolve(data)
            },
        )
    })
}