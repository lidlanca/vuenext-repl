type UncompressedEncoding = 'String' | 'ByteArray'
type CompressedEncoding =
  | 'ByteArray'
  | 'Base64'
  | 'BinaryString'
  | 'StorageBinaryString'
type DecompressedEncoding = 'String' | 'ByteArray'
type CompressionOptions = {
  inputEncoding?: UncompressedEncoding
  outputEncoding?: CompressedEncoding
  useWebWorker?: boolean
  blockSize?: number
}
type DecompressionOptions = {
  inputEncoding?: CompressedEncoding
  outputEncoding?: DecompressedEncoding
  useWebWorker?: boolean
  blockSize?: number
}
export function compress(
  input: string | Uint8Array,
  options?: CompressionOptions
): Uint8Array | string
export function decompress(
  input: Uint8Array | string,
  options?: DecompressionOptions
): string | Uint8Array
export function compressAsync(
  input: string | Uint8Array,
  options: CompressionOptions,
  callback: (result?: Uint8Array | string, error?: Error) => void
): void
export function decompressAsync(
  input: Uint8Array | string,
  options: DecompressionOptions,
  callback: (result?: string | Uint8Array, error?: Error) => void
): void

export function encodeUTF8(str: string): Uint8Array
export function decodeUTF8(input: Uint8Array): string
export function encodeBase64(input: Uint8Array): string
export function decodeBase64(str: string): Uint8Array
// function encodeBinaryString(input: Uint8Array): string;
// function decodeBinaryString(str: string): Uint8Array;
// function encodeStorageBinaryString(input: Uint8Array): string;
// function decodeStorageBinaryString(str: string): Uint8Array;
