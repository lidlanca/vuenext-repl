import { ReplStore } from "src"
import { debounceWithNotify } from '../utils'
import { ref } from "vue"


/**
 * The bridge helps connecting the Editor.vue and the Bar.vue
 * 
 * it provides 
 *  - editorChangeHandler
 *    is a handler that can be attached to the @change event <CodeMirror> component in Editor.vue
 *    it provide logic for handling changes due to actual user edits , or a file/tab change.
 *    where both actions emit @change event. 
 * 
 *  - editState
 *    ref provide the ability to know when the debounce logic notifies.
 *    it is used by the bar, to update the editing ... indicator.
 *    and notify Bar.vue when a state changes. started or ended.
 * 
 * 
 */
export function useBarBridge(store: ReplStore) {

    // handle editor state changes and debounce

    var activeFileChangeInProgress = false // 
    /**
     * set to true, to flag that the next change event from the Editor
     * is changing a file, and not changing content.
     * 
     * when the flag is set to false, change events from editor are considered content change.
     */
    function setActiveFileChangeInProgress(b: boolean) {
        activeFileChangeInProgress = b
    }

    const editorChangeHandler = function () {
        if (activeFileChangeInProgress) {  // active file change in progress, do nothing
            setActiveFileChangeInProgress(false)
        } else {                          // file content change
            onFileContentChange(...arguments)
        }
    }

    // track editor changes, so we can pass the state to Bar
    let editState = ref({ state: "", c: 0 })

    // debounce code update for compile
    const onFileContentChange:any = debounceWithNotify((code: string) => {
        store.state.activeFile.code = code
    }, store.state.editor.settings.delay, (s: string, args?) => {
        editState.value.state = s
        // c - is used to allow detection of every notification 
        // regardsless if the  state type actually changed
        // used for the . .. .. animation logic to detect consecutive debounce notifications.
        editState.value.c++
    })
    //

    return {
        editorChangeHandler,
        editState
    }

}



