import { version, reactive, watchEffect } from 'vue'
import * as defaultCompiler from '@vue/compiler-sfc'
import { compileFile } from './transform'
// import { utoa, atou } from './utils'
import storeLoader from './extended/state/storeLoader'
import { ReplStore2 } from './storeEditor'

const defaultMainFile = 'App.vue'

const welcomeCode = `
<script setup>
import { ref } from 'vue'

const msg = ref('Hello World!')
</script>

<template>
  <h1>{{ msg }}</h1>
  <input v-model="msg">
</template>
`.trim()

export class File {
  filename: string
  code: string
  compiled = {
    js: '',
    css: '',
    ssr: ''
  }

  constructor(filename: string, code = '') {
    this.filename = filename
    this.code = code
  }
}


export interface StoreState {
  mainFile: string
  files: Record<string, File>
  activeFile: File
  errors: (string | Error)[]
  vueRuntimeURL: string
}

// <extended>
function load(payload: string, store: ReplStore2) {
  return storeLoader.load(payload, store)
}
function save(payload: object) {
  return storeLoader.save(payload)
}
// </extended>



export class ReplStore {
  state: StoreState
  compiler = defaultCompiler
  defaultVueRuntimeURL: string
  pendingCompiler: Promise<any> | null = null

  constructor({
    serializedState = '',
    defaultVueRuntimeURL = `https://unpkg.com/@vue/runtime-dom@${version}/dist/runtime-dom.esm-browser.js`,
    extended = false
  }: {
    serializedState?: string
    defaultVueRuntimeURL?: string
    extended?: boolean
  } = {}) {
    let files: StoreState['files'] = {}

    this.defaultVueRuntimeURL = defaultVueRuntimeURL

    let mainFile = defaultMainFile
    if (!files[mainFile]) {
      mainFile = Object.keys(files)[0]
    }
    this.state = reactive({
      mainFile,
      files,
      activeFile: files[mainFile],
      errors: [],
      vueRuntimeURL: this.defaultVueRuntimeURL,
    })

    // if extended defer the init() call
    if (!extended) {
      this.init({ serializedState })
    }

  }

  async init({ serializedState }: {
    serializedState?: string
    defaultVueRuntimeURL?: string
    extended?: boolean
  }) {
    await this.loadSerialized(serializedState!)

    // setting the active file, after we 
    //this.state.activeFilename = 'App.vue'
  }

  setActive(filename: string) {
    this.state.activeFile = this.state.files[filename]
  }

  addFile(filename: string) {
    this.state.files[filename] = new File(filename)
    this.setActive(filename)
  }

  deleteFile(filename: string) {
    if (confirm(`Are you sure you want to delete ${filename}?`)) {
      if (this.state.activeFile.filename === filename) {
        this.state.activeFile = this.state.files[this.state.mainFile]
      }
      delete this.state.files[filename]
    }
  }

  serialize() {
    // <extended>
    let addr = save(this.getFiles())
    return "#" + addr
    // </extended>
    // return '#' + utoa(JSON.stringify(this.getFiles()))
  }

  getFiles() {
    const exported: Record<string, string> = {}
    for (const filename in this.state.files) {
      exported[filename] = this.state.files[filename].code
    }
    return exported
  }

  async setFiles(newFiles: Record<string, string>, mainFile = defaultMainFile) {
    const files: Record<string, File> = {}
    if (mainFile === defaultMainFile && !newFiles[mainFile]) {
      files[mainFile] = new File(mainFile, welcomeCode)
    }
    for (const filename in newFiles) {
      files[filename] = new File(filename, newFiles[filename])
    }
    for (const file in files) {
      await compileFile(this, files[file])
    }
    this.state.mainFile = mainFile
    this.state.files = files
    this.initImportMap()
    this.setActive(mainFile)
  }

  private initImportMap() {
    const map = this.state.files['import-map.json']
    if (!map) {
      this.state.files['import-map.json'] = new File(
        'import-map.json',
        JSON.stringify(
          {
            imports: {
              vue: this.defaultVueRuntimeURL
            }
          },
          null,
          2
        )
      )
    } else {
      try {
        const json = JSON.parse(map.code)
        if (!json.imports.vue) {
          json.imports.vue = this.defaultVueRuntimeURL
          map.code = JSON.stringify(json, null, 2)
        }
      } catch (e) {}
    }
  }

  getImportMap() {
    try {
      return JSON.parse(this.state.files['import-map.json'].code)
    } catch (e) {
      this.state.errors = [
        `Syntax error in import-map.json: ${(e as Error).message}`
      ]
      return {}
    }
  }

  setImportMap(map: {
    imports: Record<string, string>
    scopes?: Record<string, Record<string, string>>
  }) {
    this.state.files['import-map.json']!.code = JSON.stringify(map, null, 2)
  }

  async setVueVersion(version: string) {
    const compilerUrl = `https://unpkg.com/@vue/compiler-sfc@${version}/dist/compiler-sfc.esm-browser.js`
    const runtimeUrl = `https://unpkg.com/@vue/runtime-dom@${version}/dist/runtime-dom.esm-browser.js`
    this.pendingCompiler = import(/* @vite-ignore */ compilerUrl)
    this.compiler = await this.pendingCompiler
    this.pendingCompiler = null
    this.state.vueRuntimeURL = runtimeUrl
    const importMap = this.getImportMap()
    ;(importMap.imports || (importMap.imports = {})).vue = runtimeUrl
    this.setImportMap(importMap)
    console.info(`[@vue/repl] Now using Vue version: ${version}`)
  }

  resetVueVersion() {
    this.compiler = defaultCompiler
    this.state.vueRuntimeURL = this.defaultVueRuntimeURL
  }



  // <extended>  logic extracted from constructor
  // 
  private async loadSerialized(serializedState: string) {
    const self = this


    self.initImportMap()

    if (serializedState) {
      // plugins are supported in ReplStore2 
      const _saved = await load(serializedState, this as unknown as ReplStore2)

      let data = _saved.d
      for (const filename in data) {
        this.state.files[filename] = new File(filename, data[filename])
      }
    } else {
      this.state.files['App.vue'] = new File(defaultMainFile, welcomeCode)
    }

    watchEffect(() => {
      // guard when there is no active file don't try to compile undefined 
      if (self.state.activeFile) {
        compileFile(self, self.state.activeFile)
      }
    })

    for (const file in this.state.files) {
      if (file !== defaultMainFile) {
        compileFile(this, this.state.files[file])
      }
    }

  }

}
// </extended>
