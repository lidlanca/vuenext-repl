export function debounce(fn: Function, n = 100) {
  let handle: any
  return (...args: any[]) => {
    if (handle) clearTimeout(handle)
    handle = setTimeout(() => {
      fn(...args)
    }, n)
  }
}

// prefer old unicode hacks for backward compatibility
// https://base64.guru/developers/javascript/examples/unicode-strings
export function utoa(data: string): string {
  return btoa(unescape(encodeURIComponent(data)))
}

export function atou(base64: string): string {
  return decodeURIComponent(escape(atob(base64)))
}




export type state = 'start' | 'end' | 'bounce' | 'executing' | 'cancel'

interface CancellableFunc {
  cancel: Function
  immediate: Function
}

/**
 * debounceWithNotify create a cancellable debounce function with a state notify callback.
 * Notify statuses:
 *   start     - debounce started
 *  bounce    - bounce call
 *  executing - before executing fn
 *  end       - debounce function completed
 *  cancel    - cancel was called
 *
 *  The debounce function, exposes:
 *  .cancel()        cancel the pending debounced call.
 *  .immediate()     execute the pending debounced call immediately.
 *
 */
export function debounceWithNotify(
  fn: (...args: any[]) => any,
  n: number = 100,
  notify: (s: state,args?:any) => void
): CancellableFunc {
  var handle: any = 0
  var latest: () => any
  var cancel: () => void
  var immediate: () => void

  cancel = function() {
    if (handle) {
      notify('cancel')
      clearTimeout(handle)
      handle = 0
    }
  }

  immediate = function() {
    if (handle) {
      clearTimeout(handle)
      handle = 0
      execute()
    }
  }

  var execute: () => void = function() {
    notify('executing')
    latest()
    notify('end')
    handle = 0
  }

  var t = (function(...args: any[]) {
    if (!handle) {
      notify('start',args)
    } else {
      clearTimeout(handle)
      notify('bounce',args)
    }
    latest = () => fn(...args)
    handle = setTimeout(() => {
      execute()
    }, n)
  } as unknown) as CancellableFunc

  t.cancel = cancel
  t.immediate = immediate
  return t
}
