//import StoreLoader from './storeLoader'

//import { fromPersist, toPersist, mergeItem, editorSettingsStorageKey } from './localPresist'

import { ReplStore } from './store'


import { loadGist } from './extended/gist/loader/gistLoader'

import type { StoreState } from "./store"


/**
 * the storeEditor extends the .store.ts
 * 
 * ReplStore to support editor settings.
 * we extract the logic here, so we don't have to deal with ReplStore base changes.
 * 
 */



//    editor: { settings: { delay: 750, darkMode: true}}

interface StoreStateEditor extends StoreState {
    editor: { settings: EditorStoreSettings }
}

export { StoreStateEditor as StoreState }


export type EditorStoreSettings = {
    delay: number,
    darkMode: boolean,
}


interface Loader {
    load: (id: string) => Promise<any>
}
export interface GithubGistLoaderPlugin extends Loader {
    load: typeof loadGist
}

type plugins = {
    loaders: {
        [k: string]: Loader
    }
}
export class ReplStore2 extends ReplStore {
    setEditorDarkMode: (on: boolean) => any
    getEditorDarkMode: () => boolean
    state!: StoreStateEditor
    plugins: plugins

    constructor(arg: any) {
        // extended flag, indicate to bae ReplStore that it is being extended. so that we can defer the init() call
        super({ ...arg, extended: true })

        // extend plugins 
        this.plugins = {
            loaders: {
                'github': { load: loadGist }
            }
        }

        // extend  editor settings
        this.state.editor = { settings: { darkMode: true, delay: 750 } }

        this.setEditorDarkMode = (on: boolean) => {
            this.state.editor.settings.darkMode = on
        }
        this.getEditorDarkMode = (): boolean => {
            return this.state.editor.settings.darkMode
        }

        // finally call the init
        this.init(arg)

    }
    getLoader(name: string): Loader {
        let loader = this.plugins.loaders[name]
        return loader
    }

}

